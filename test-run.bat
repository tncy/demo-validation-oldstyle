@echo off
cls
@if "%OS%" == "Windows_NT" (
	setlocal
	set "DIRNAME=%~dp0%"
) else (
	set DIRNAME=.\
)

call "%DIRNAME%common.bat" :WORKSPACE

:SET_CLASSPATH
rem Libraries
set HIBVAL_DIST=%EXT%\hibernate-validator-6.2.0.Final\dist
set HIBVAL_REQUIRED=%HIBVAL_DIST%\lib\required\*

if "%1"=="-jar" goto RUN_FROM_JAR

:RUN_FROM_COMPILED_CLASSES
set CLASSPATH=%COMPILED_TEST_CLASSES%;%COMPILED_CLASSES%;%HIBVAL_DIST%\hibernate-validator-6.2.0.Final.jar;%HIBVAL_REQUIRED%;%JAVA_LIBS%\junit-4.13.2.jar;%JAVA_LIBS%\hamcrest-core-1.3.jar

java -cp %CLASSPATH% org.junit.runner.JUnitCore net.tncy.demo.validation.PersonValidationTest

goto END

:RUN_FROM_JAR
set CLASSPATH=%COMPILED_TEST_CLASSES%;%DIST%\demo.jar;%HIBVAL_DIST%\hibernate-validator-6.2.0.Final.jar;%HIBVAL_REQUIRED%;%JAVA_LIBS%\junit-4.13.2.jar;%JAVA_LIBS%\hamcrest-core-1.3.jar

java -cp %CLASSPATH% org.junit.runner.JUnitCore net.tncy.demo.validation.PersonValidationTest

goto END

:END
endlocal