@echo off
cls
@if "%OS%" == "Windows_NT" (
	setlocal
	set "DIRNAME=%~dp0%"
) else (
	set DIRNAME=.\
)

call "%DIRNAME%common.bat" :WORKSPACE

if "%1"=="-clean" goto CLEAN

:SET_CLASSPATH
rem Libraries
set HIBVAL_DIST=%EXT%\hibernate-validator-6.2.0.Final\dist
set HIBVAL_REQUIRED=%HIBVAL_DIST%\lib\required\*

set CLASSPATH=%COMPILED_CLASSES%;%HIBVAL_DIST%\lib\required\*;%JAVA_LIBS%\junit-4.13.2.jar


:COMPILE
call "%DIRNAME%common.bat" :PREPARE_TEST_COMPILE

echo Compiling test sources
type %BUILD%\test_sources.list
javac -cp %CLASSPATH% -d %COMPILED_TEST_CLASSES% @%BUILD%\test_sources.list

goto END

:CLEAN
call "%DIRNAME%common.bat" :CLEAN

goto SET_CLASSPATH

:END
endlocal