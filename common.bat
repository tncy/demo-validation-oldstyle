rem @echo off
call %*
goto :eof

:WORKSPACE
set SOURCES=src
set TEST_SOURCES=test
set BUILD=build
set COMPILED_CLASSES=%BUILD%\classes
set COMPILED_TEST_CLASSES=%BUILD%\test
set DIST=dist
set JAVADOC=%DIST%\javadoc
set EXT=C:\Opt
set JAVA_LIBS=%EXT%\lib\java

goto :eof

:CLEAN
echo Cleaning workspace
if exist "%BUILD%" (
	rd /S /Q %BUILD%
)
if exist "%DIST%" (
	rd /S /Q %DIST%
)

goto :eof

:CLEAN_JAVADOC
echo Cleaning JavaDoc
if exist "%JAVADOC%" (
	rd /S /Q %JAVADOC%
)

goto :eof

:PREPARE_COMPILE
if not exist "%BUILD%" (
	md %BUILD%
)
if not exist "%COMPILED_CLASSES%" (
	md %COMPILED_CLASSES%
)
rem List all classes to compile
dir /s /B %SOURCES%\*.java > %BUILD%\sources.list

goto :eof

:PREPARE_TEST_COMPILE
if not exist "%BUILD%" (
	md %BUILD%
)
if not exist "%COMPILED_TEST_CLASSES%" (
	md %COMPILED_TEST_CLASSES%
)
rem List classes to compile
dir /s /B %TEST_SOURCES%\*.java > %BUILD%\test_sources.list

goto :eof

:PREPARE_DIST
if not exist "%DIST%" (
	md %DIST%
)

goto :eof

:PREPARE_JAVADOC
if not exist "%BUILD%" (
	md %BUILD%
)
dir /s /B %SOURCES%\*.java > %BUILD%\sources.list
if not exist "%JAVADOC%" (
	md %JAVADOC%
)

goto :eof

:END
