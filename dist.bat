@echo off
cls
@if "%OS%" == "Windows_NT" (
	setlocal
	set "DIRNAME=%~dp0%"
) else (
	set DIRNAME=.\
)

call "%DIRNAME%common.bat" :WORKSPACE

if "%1"=="-clean" goto CLEAN

:COMPILE
call "%DIRNAME%compile.bat" :SET_CLASSPATH

goto PACKAGE

:PACKAGE
call "%DIRNAME%common.bat" :PREPARE_DIST
echo Creating JAR
"%JAVA_HOME%\bin\jar" cfe %DIST%\demo.jar net.tncy.demo.validation.Demo -C %COMPILED_CLASSES%/ .
echo %DIST%\demo.jar

goto END

:CLEAN
call "%DIRNAME%common.bat" :CLEAN

goto COMPILE

:END
endlocal