@echo off
cls
@if "%OS%" == "Windows_NT" (
	setlocal
	set "DIRNAME=%~dp0%"
) else (
	set DIRNAME=.\
)

call "%DIRNAME%common.bat" :WORKSPACE

if "%1"=="-clean" goto CLEAN

:PREPARE_JAVADOC
call "%DIRNAME%common.bat" :PREPARE_DIST
call "%DIRNAME%common.bat" :PREPARE_JAVADOC

:SET_CLASSPATH
rem Libraries
set HIBVAL_DIST=%EXT%\hibernate-validator-6.2.0.Final\dist
set HIBVAL_REQUIRED=%HIBVAL_DIST%\lib\required\*

:RUN
set CLASSPATH=%COMPILED_CLASSES%;%HIBVAL_DIST%\hibernate-validator-6.2.0.Final.jar;%HIBVAL_REQUIRED%
echo Generating JavaDoc

"%JAVA_HOME%\bin\javadoc" -quiet -private -d %JAVADOC% -sourcepath %SOURCES% -subpackages net

goto END

:CLEAN
call "%DIRNAME%common.bat" :CLEAN_JAVADOC

goto PREPARE_JAVADOC @%BUILD%\sources.list

:END
endlocal