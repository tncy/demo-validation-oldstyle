@echo off
cls
@if "%OS%" == "Windows_NT" (
	setlocal
	set "DIRNAME=%~dp0%"
) else (
	set DIRNAME=.\
)

call "%DIRNAME%common.bat" :WORKSPACE

if "%1"=="-clean" goto CLEAN

:SET_CLASSPATH
rem Libraries
set HIBVAL_DIST=%EXT%\hibernate-validator-6.2.0.Final\dist
set HIBVAL_REQUIRED=%HIBVAL_DIST%\lib\required\*

set CLASSPATH=%HIBVAL_REQUIRED%

:COMPILE
call "%DIRNAME%common.bat" :PREPARE_COMPILE

echo Compiling sources
type %BUILD%\sources.list
javac -cp %CLASSPATH% -d %COMPILED_CLASSES% @%BUILD%\sources.list

goto END

:CLEAN
call "%DIRNAME%common.bat" :CLEAN

goto SET_CLASSPATH

:END
endlocal