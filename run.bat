@echo off
cls
@if "%OS%" == "Windows_NT" (
	setlocal
	set "DIRNAME=%~dp0%"
) else (
	set DIRNAME=.\
)

call "%DIRNAME%common.bat" :WORKSPACE

:SET_CLASSPATH
rem Libraries
set HIBVAL_DIST=%EXT%\hibernate-validator-6.2.0.Final\dist
set HIBVAL_REQUIRED=%HIBVAL_DIST%\lib\required\*

if "%1"=="-jar" goto RUN_FROM_JAR

:RUN_FROM_COMPILED_CLASESS
set CLASSPATH=%COMPILED_CLASSES%;%HIBVAL_DIST%\hibernate-validator-6.2.0.Final.jar;%HIBVAL_REQUIRED%

java -cp %CLASSPATH% net.tncy.demo.validation.Demo

goto END

:RUN_FROM_JAR
set CLASSPATH=%DIST%\demo.jar;%HIBVAL_DIST%\hibernate-validator-6.2.0.Final.jar;%HIBVAL_REQUIRED%

java -cp %CLASSPATH% net.tncy.demo.validation.Demo

goto END

:END
endlocal